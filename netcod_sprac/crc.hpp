#pragma once

#include <algorithm>
#include <array>
#include <cstdint>
#include <numeric>

// These headers are only needed for main(), to demonstrate.
#include <iomanip>
#include <iostream>
#include <string>

class crc{
public:
    crc();
    ~crc();
    std::array<std::uint_fast32_t, 256> generate_crc_lookup_table() noexcept;
    template <typename InputIterator> std::uint_fast32_t crc32(InputIterator first, InputIterator last);
    uint32_t crc8(const uint8_t *data, uint8_t len);
};

crc::crc(){ }

crc::~crc(){ }

uint32_t crc::crc8(const uint8_t *data, uint8_t len)
{
  uint8_t crc = 0x00;

    while (len--)
    {
        uint8_t extract = *data++;

        for (uint8_t tempI = 8; tempI; tempI--)
        {
            uint8_t sum = (crc ^ extract) & 0x01;
            crc >>= 1;

            if (sum)
            {
               // crc ^= 0x8C; // CRC-8 DALLAS/MAXIM
               crc ^= 0xD9; // CRC-8 WCDMA
            }
            extract >>= 1;
        }
    }

  return crc;

}

std::array<std::uint_fast32_t, 256> crc::generate_crc_lookup_table() noexcept
{
  auto const reversed_polynomial = std::uint_fast32_t{0xEDB88320uL};

  // This is a function object that calculates the checksum for a value,
  // then increments the value, starting from zero.
  struct byte_checksum
  {
    std::uint_fast32_t operator()() noexcept
    {
      auto checksum = static_cast<std::uint_fast32_t>(n++);

      for (auto i = 0; i < 8; ++i)
        checksum = (checksum >> 1) ^ ((checksum & 0x1u) ? reversed_polynomial : 0);

      return checksum;
    }

    unsigned n = 0;
  };

  auto table = std::array<std::uint_fast32_t, 256>{};
  std::generate(table.begin(), table.end(), byte_checksum{});

  return table;
}

template <typename InputIterator>
std::uint_fast32_t crc::crc32(InputIterator first, InputIterator last)
{
  // Generate lookup table only on first use then cache it - this is thread-safe.
  static auto const table = this->crc::generate_crc_lookup_table();

  // Calculate the checksum - make sure to clip to 32 bits, for systems that don't
  // have a true (fast) 32-bit type.
  return std::uint_fast32_t{0xFFFFFFFFuL} &
    ~std::accumulate(first, last,
      ~std::uint_fast32_t{0} & std::uint_fast32_t{0xFFFFFFFFuL},
        [](std::uint_fast32_t checksum, std::uint_fast8_t value)
          { return table[(checksum ^ value) & 0xFFu] ^ (checksum >> 8); });
}