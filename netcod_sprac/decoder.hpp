#pragma once

#include "utils.hpp"
#include <cstdint>
#include <vector>
#include <iostream>

#include <fifi/api/create_default_field.hpp>

typedef std::vector<uint8_t> V1D;
typedef std::vector<V1D> V2D;

class decoder{
public:
    decoder();
    ~decoder();

    V2D decode(V2D &);
};

decoder::decoder() { }

decoder::~decoder() { }

V2D decoder::decode(V2D &m)
{
    auto field = fifi::api::create_default_field(fifi::api::field::binary);

    utils utils;

    uint32_t row = m.size();
    uint32_t col = m[0].size();

    uint32_t crc_size = 32;

    V2D encoded(row, V1D(col-row));
    V2D coef_encoded(row, V1D(col-crc_size));
    V2D coef(row, V1D(row));
    V2D uncoded(row, V1D(col-row-crc_size));
    V2D crc_m(row, V1D(crc_size));

    // fill coef_encoded matrix with coeficient vector + encoded packets
    for(size_t i=0; i < row; ++i){
        for(size_t j=0; j < col-crc_size; ++j){
            coef_encoded[i][j] = m[i][j];
        }
    }

    // fill coef matrix with coeficient vector
    for(size_t i=0; i < row; ++i){
        for(size_t j=0; j < row; ++j){
            coef[i][j] = m[i][j];
        }
    }

    // std::cout << "coding vector: " << std::endl;
    // for(size_t k=0; k < row; ++k)
    // {
    //     for(size_t l=0; l < row ; ++l)
    //     {
    //         std::cout << +coef[k][l];
    //     }
    //     std::cout << std::endl;
    // }

    // fill encoded matrix with encoded packet + crc
    for(size_t i=0; i < row; ++i){
        for(size_t j=row; j < col; ++j){
            encoded[i][j-row] = m[i][j];
        }
    }

    // std::cout << "encoded vector: " << std::endl;
    // for(size_t k=0; k < row; ++k)
    // {
    //     for(size_t l=0; l < col-row ; ++l)
    //     {
    //         std::cout << +encoded[k][l];
    //     }
    //     std::cout << std::endl;
    // }

    // fill crc_m matrix with crc32
    for(size_t i=0; i < row; ++i){
        for(size_t j=col-crc_size; j < col; ++j){
            crc_m[i][j-(col-crc_size)] = m[i][j];
        }
    }

    V1D crc_correct(row);

    crc_correct = utils.check_crc(coef_encoded,crc_m);

    std::string status;
    for(int i=0; i < crc_correct.size(); ++i){
        if(crc_correct[i] == 1)
            status = "Correct";
        else
            status = "False";

        std::cout << "CRC Packet-" << i << ": " << status << std::endl;
    }
    std::cout << std::endl;

    // resize encoded matrix and delete the crc
    for(size_t i=0; i < row; ++i){
        encoded[i].resize(col-row-crc_size);
    }

    V2D inv_coef(row, V1D(row));

    inv_coef = utils.calculate_inverse(coef);

    std::cout << "inverse vector: " << std::endl;
    for(size_t k=0; k < row; ++k){
        for(size_t l=0; l < row ; ++l){
            std::cout << +inv_coef[k][l];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    uint32_t rowc = coef.size();
    uint32_t colc = coef[0].size();
    uint32_t rowp = uncoded.size();
    uint32_t colp = uncoded[0].size();

    // Matrix multiplication over GF(2) to get uncoded packet
    uint32_t coe,cod, ori;
    for(size_t i = 0; i < rowc; ++i){
        for(size_t j = 0; j < colp; ++j){
            for(size_t k = 0; k < colc; ++k){
                uncoded[i][j] = field->add(uncoded[i][j], field-> multiply(inv_coef[i][k], encoded[k][j]));
            }
        }
    }

    return uncoded;
}