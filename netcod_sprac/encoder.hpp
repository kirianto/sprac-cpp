#pragma once

#include <vector>
#include <cstdint>
#include <iostream>

#include "utils.hpp"
#include "crc.hpp"

#include <fifi/api/create_default_field.hpp>

typedef std::vector<uint8_t> V1D;
typedef std::vector<V1D> V2D;

class encoder{
public:
    encoder();
    ~encoder();
    V2D encode(V2D &, V2D &, uint32_t &);

};

encoder::encoder() { }

encoder::~encoder() { }

// &c: coefficient packets in 2D vector
// &p: uncoded/original packets in 2D vector
// the result/output is coef + encoded + crc32 packets in 2D vector
V2D encoder::encode(V2D &c, V2D &p, uint32_t &seg_size)
{
    auto field = fifi::api::create_default_field(fifi::api::field::binary);

    utils utils;
    crc crc;

    uint32_t rowc = c.size(); // rows of coefficient vectors
    uint32_t colc = c[0].size(); // columns of coefficient vectors
    uint32_t rowp = p.size(); // rows of uncoded vectors
    uint32_t colp = p[0].size(); // columns of uncoded vectors
    uint32_t crc_size = 32; // in bits

    // create encoded vectors
    V2D m(rowc, V1D(colp));
    V2D m_crc8;

    // Matrix multiplication over GF(2) between coding vector and
    // uncoded packets to form encoded packets
    uint32_t coe,cod, ori; // buffer for coefficient, coded, original
    for(size_t i = 0; i < rowc; ++i)
        for(size_t j = 0; j < colp; ++j)
            for(size_t k = 0; k < colc; ++k)
            {
                if(p[k][j] == 48)
                    ori = 0;
                else
                    ori = 1;

                coe = c[i][k];
                cod = m[i][j];

                m[i][j] = field->add(cod, field-> multiply(coe, ori));
            }

    // add crc8 to encoded packets of each segment
    m_crc8 = utils.add_crc8(m, seg_size);

    // create coefficient + encoded vectors with crc8
    V2D coef_m(rowc, V1D(colc+m_crc8[0].size(),0));

    // fill it with coeffiecient vector
    for(size_t i=0; i < coef_m.size(); ++i)
        for(size_t j=0; j < colc; ++j)
            coef_m[i][j] = c[i][j];

    // fill it with encoded vectors
    for(size_t i=0; i < coef_m.size(); ++i)
        for(size_t j=colc; j < coef_m[i].size(); ++j)
            coef_m[i][j] = m_crc8[i][j-colc];

    // check crc of coef + encoded with crc8 matrix
    uint32_t int_crc;

    std::string str_crc;

    // create crc32 for coef + encoded vectors
    V1D crc_m;

    // create buffer for coef_m vectors
    V1D temp(coef_m[0].size());

    for(size_t i=0; i < coef_m.size(); ++i){
        for(size_t j=0; j < coef_m[0].size(); ++j){
            temp[j] = (coef_m[i][j]);
        }

        for(auto i=0;i < temp.size();++i){
            if(temp[i] == 0)
                temp[i] = 48;
            else if(temp[i] == 1)
                temp[i] = 49;
        }

        // check crc32 of coef + encoded vectors
        // store it in int_crc for converting to binary
        int_crc = crc.crc32(temp.begin(), temp.end());
        str_crc = utils.int_to_binary(int_crc);

        for(auto s: str_crc){
            if(s==48)
                s = 0;
            else
                s = 1;

            // store binary crc to crc_m vectors
            crc_m.push_back(s);
        }
    }

    // resize ceof_m to cover the length of crc32
    for(size_t i=0; i < coef_m.size(); ++i){
        coef_m[i].resize(coef_m[i].size()+crc_size);
    }

    // insert crc32 to coef_m  so that it consist of
    // coeficient + encoded + crc32
    int k=0;
    for(size_t i=0; i < rowc; ++i){
        for(size_t j=coef_m[i].size()-crc_size; j < coef_m[i].size(); ++j){
            coef_m[i][j] = crc_m[k];
            ++k;
        }
    }
    // return coef + encoded with crc8 + crc32 vectors
    return coef_m;
}

