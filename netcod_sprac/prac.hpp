#pragma once

#include <vector>
#include <cstdint>
#include <algorithm>
#include <iostream>
#include <numeric>
#include "utils.hpp"
#include "utils_prac.hpp"
#include "crc.hpp"
#include <chrono>

#include <fifi/api/create_default_field.hpp>

typedef std::vector<uint8_t> V1D;
typedef std::vector<V1D> V2D;
typedef std::chrono::duration<double, std::milli> T_milli;

using namespace std;

class prac{
public:
    prac();
    ~prac();
    std::tuple<V2D, T_milli, T_milli> decode(V2D &, V1D &, uint32_t &, uint32_t &, uint32_t &);
    V2D guess_uncoded(V2D &, uint32_t &);
    V2D guess_uncoded_seg(V2D &, V2D &);
    V1D guess_encode_seg(V1D &,V2D &);
    V1D compare_pkt(V1D &, V1D &);
    V1D correction_seg(V1D &, V1D &, V1D &, V1D &);
    // V1D guess_encode(V1D &,V2D &);
    // V1D correction(V1D &, V1D &, V1D &, V1D &);
};

prac::prac(){ }

prac::~prac(){ }

std::tuple<V2D, T_milli, T_milli> prac::decode(V2D &m, V1D &crc, uint32_t &sym, uint32_t &sym_size, uint32_t &seg_size)
{
    utils_prac utils_prac;

    uint32_t row = m.size();
    uint32_t col = m[0].size();
    uint32_t crc_size = 32;
    uint32_t crc8_size = 8;
    uint32_t num_seg = (col-crc_size-sym) / (seg_size+crc8_size);
    uint32_t num_bit_err;
    uint32_t num_corr_crc;

    V1D pkt_check;
    V1D correct_crc8;
    V2D enc_2d;
    V2D coef_m_seg;
    V2D coef_m(row, V1D(sym));
    V1D coef(sym);
    V1D m_1(seg_size);
    V1D crc8_m_1(crc8_size);
    V1D buffer;
    V2D m_crc8(sym, V1D(seg_size+crc8_size));
    V2D m_seg(row, V1D(seg_size+crc8_size));

    V2D temp_pkt_false;

    std::chrono::duration<double, std::milli> tot_err_loc(0);
    std::chrono::duration<double, std::milli> tot_corr(0);

    for(auto j=0; j < num_seg; ++j){


        int k=0;

        for(auto i=0; i<m.size(); ++i){
            for(auto k=(j*(seg_size+crc8_size))+sym; k<((j+1)*(seg_size+crc8_size))+sym; ++k){
                buffer.push_back(m[i][k]);
            }
        }

        for(auto i=0;i<m_seg.size();++i){
            for(auto j=0;j<m_seg[i].size();++j){
                m_seg[i][j] = buffer[k];
                ++k;
            }
        }

        for(auto x=0; x<row; ++x){
            for(auto y=0; y<sym; ++y){
                coef_m[x][y] = m[x][y];
            }
        }

        correct_crc8 = utils_prac.check_crc8_prac(m_seg);

        uint32_t num_corr_crc_seg = std::accumulate(correct_crc8.begin(), correct_crc8.end(), 0);

        if(num_corr_crc_seg != m.size()){

            auto start_err_loc = std::chrono::high_resolution_clock::now();

            // cout << "segment: " << j << endl;

            // cout << "correct_crc8: ";
            // for(auto s : correct_crc8)
            //     cout << +s;
            // cout << endl;

            for(int i=0; i < correct_crc8.size(); ++i){
                if(correct_crc8[i]==0){
                    pkt_check.push_back(i);
                }
            }

            if(!pkt_check.empty()){

                for(int i=0; i<pkt_check.size(); ++i){

                    enc_2d = m_seg;
                    coef_m_seg = coef_m;

                    for(int s=0; s < m.size(); ++s){
                        for(int o=0; o < sym; ++o){
                            if(pkt_check[i] == s)
                                coef[o] = m[s][o];
                        }
                    }

                    for(int r=0; r<m_seg.size();++r){
                        for(int q=0; q<m_seg[r].size()-crc8_size;++q){
                            if(pkt_check[i] == r)
                                m_1[q] = m_seg[r][q];
                        }

                        for(int c=(m_seg[r].size()-crc8_size); c<m_seg[r].size(); ++c){
                            if(pkt_check[i] == r){
                                crc8_m_1[c-(m_seg[r].size()-crc8_size)] = m_seg[r][c];
                            }
                        }
                    }

                    enc_2d.erase(enc_2d.begin()+pkt_check[i]);
                    coef_m_seg.erase(coef_m_seg.begin()+pkt_check[i]);

                    // cout << "check pkt: "<< +pkt_check[i] << " | " << "Coef pkt: ";
                    // for(auto s : coef)
                    //     cout << +s;
                    // cout << endl;

                    // cout << "m_1: ";
                    // for(auto s : m_1)
                    //     cout << +s;
                    // cout << endl;

                    // cout << "crc8_m_1: ";
                    // for(auto s : crc8_m_1)
                    //     cout << +s;
                    // cout << endl;

                    V2D guess_uncoded;
                    V1D guess_encoded;
                    V1D error_location;

                    guess_uncoded = this->guess_uncoded_seg(coef_m_seg, enc_2d);
                    guess_encoded = this->guess_encode_seg(coef, guess_uncoded);

                    // std::cout <<"guess_encoded: "<<+pkt_check[i]<<std::endl;
                    // for(auto s : guess_encoded)
                    //     std::cout << +s;
                    // std::cout <<"\n"<< std::endl;

                    error_location = this->compare_pkt(guess_encoded, m_1);

                    auto end_err_loc = std::chrono::high_resolution_clock::now();

                    auto diff_err_loc = end_err_loc - start_err_loc;

                    tot_err_loc += diff_err_loc;

                    // std::cout <<"error_location: ";
                    // for(auto s : error_location)
                    //     std::cout << +s;
                    // std::cout <<"\n"<< std::endl;

                    num_bit_err = std::accumulate(error_location.begin(), error_location.end(), 0);

                    if(num_bit_err == 0){
                        for(auto i=0; i<error_location.size();++i){
                            error_location[i] = 1;
                        }
                    }

                    auto start_corr = std::chrono::high_resolution_clock::now();

                    temp_pkt_false.push_back(this->correction_seg(coef, m_1, crc8_m_1, error_location));

                    auto end_corr = std::chrono::high_resolution_clock::now();

                    auto diff_corr = end_corr - start_corr;
                    tot_corr += diff_corr;
                }

                int it = 0;
                for(int i=0; i<correct_crc8.size(); ++i){
                    if(correct_crc8[i] != 1){
                        for(auto j=0; j<m_seg[i].size();++j){
                            m_seg[i][j] = temp_pkt_false[it][j];
                        }
                        ++it;
                    }
                }
                it=0;


                // std::cout<<"Correct encoded Packet: " << std::endl;
                // for(auto i=0; i< m_seg.size();++i){
                //     for(auto j=0;j<m_seg[i].size(); ++j){
                //         std::cout << +m_seg[i][j];
                //     }
                //     std::cout << std::endl;
                // }

                for(auto i=0; i<m.size(); ++i){
                    for(auto k=(j*(seg_size+crc8_size))+sym; k<((j+1)*(seg_size+crc8_size))+sym; ++k){
                        m[i][k] = m_seg[i][k-((j*(seg_size+crc8_size))+sym)];
                    }
                }
                // std::cout << std::endl;
                temp_pkt_false.clear();
            }
        }

        buffer.clear();
        pkt_check.clear();
    }

    auto check_crc32 = utils_prac.check_crc_prac(m);
    // cout << "check_crc32: ";
    // for(auto s: check_crc32)
    //     cout << +s;
    // cout << endl;

    num_corr_crc = std::accumulate(check_crc32.begin(),check_crc32.end(),0);

    if(num_corr_crc == m.size()){
        // cout << "encoded packet after correction: " << endl;
        // for(auto i=0;i<m.size();++i){
        //     for(auto j=0;j<m[i].size();++j){
        //         cout << +m[i][j];
        //     }
        //     cout << endl;
        // }

        return std::make_tuple(m, tot_err_loc, tot_corr);

    }else{
        cout <<"failed to decode with PRAC!" << endl;
    }
}

V2D prac::guess_uncoded_seg(V2D &coef_m, V2D &m_seg)
{
    auto field = fifi::api::create_default_field(fifi::api::field::binary);

    utils utils;

    uint32_t row = m_seg.size();
    uint32_t col = m_seg[0].size();

    uint32_t crc_size = 8;

    V2D encoded(row,V1D(col-crc_size));
    V2D uncoded(row,V1D(col-crc_size));

    // std::cout << "coding vector: " << std::endl;
    // for(size_t k=0; k < row; ++k){
    //     for(size_t l=0; l < row ; ++l){
    //         std::cout << +coef_m[k][l];
    //     }
    //     std::cout << std::endl;
    // }

    // fill encoded matrix with encoded packet
    for(size_t i=0; i < row; ++i){
        for(size_t j=0; j < col-crc_size; ++j){
            encoded[i][j] = m_seg[i][j];
        }
    }

    // std::cout << "encoded vector: " << std::endl;
    // for(size_t k=0; k < encoded.size(); ++k){
    //     for(size_t l=0; l < encoded[k].size(); ++l){
    //         std::cout << +encoded[k][l];
    //     }
    //     std::cout << std::endl;
    // }

    V2D inv_coef(row,V1D(row));

    inv_coef = utils.calculate_inverse(coef_m);

    // std::cout << "inverse vector: " << std::endl;
    // for(size_t k=0; k < row; ++k){
    //     for(size_t l=0; l < row; ++l){
    //         std::cout << +inv_coef[k][l];
    //     }
    //     std::cout << std::endl;
    // }
    // std::cout << std::endl;

    uint32_t rowc = coef_m.size();
    uint32_t colc = coef_m[0].size();
    uint32_t rowp = uncoded.size();
    uint32_t colp = uncoded[0].size();

    // Matrix multiplication over GF(2) to get uncoded packet
    uint32_t coe,cod, ori;
    for(size_t i = 0; i < rowc; ++i)
    {
        for(size_t j = 0; j < colp; ++j)
        {
            for(size_t k = 0; k < colc; ++k)
            {
                uncoded[i][j] = field->add(uncoded[i][j], field-> multiply(inv_coef[i][k], encoded[k][j]));
            }
        }
    }

    // std::cout << std::endl;
    // std::cout << "Guessed original packets: " << std::endl;
    // for(size_t k=0; k < uncoded.size(); ++k){
    //     for(size_t l=0; l < uncoded[0].size() ; ++l){
    //         std::cout << +uncoded[k][l];
    //     }
    //     std::cout << std::endl;
    // }
    // std::cout << std::endl;

    return uncoded;
}

V1D prac::guess_encode_seg(V1D &c, V2D &p)
{
    auto field = fifi::api::create_default_field(fifi::api::field::binary);

    uint32_t rowc = 1;
    uint32_t colc = c.size();

    uint32_t rowp = p.size();
    uint32_t colp = p[0].size();

    V1D m(colp);

    // Matrix multiplication over GF(2) between coding vector and
    // uncoded packets to form encoded packets
    uint32_t coe,cod, ori;
    for(size_t i = 0; i < rowc; ++i)
        for(size_t j = 0; j < colp; ++j)
            for(size_t k = 0; k < rowp; ++k)
            {
                // if(p[k][j] == 48)
                //     ori = 0;
                // else
                //     ori = 1;
                ori = p[k][j];

                coe = c[k];
                cod = m[j];

                m[j] = field->add(cod, field-> multiply(coe, ori));
            }

    // std::cout << "guess m packet: " << std::endl;
    // for(size_t k=0; k < m.size(); ++k)
    // {
    //     std::cout << +m[k];
    // }
    // std::cout <<"\n"<< std::endl;

    return m;
}

V1D prac::correction_seg(V1D &coef, V1D &m, V1D &crc_m, V1D &error_location)
{
    crc crc;
    utils utils;
    uint32_t crc_size = 8;

    int n = std::accumulate(error_location.begin(), error_location.end(),0);
    V1D temp(n);
    V1D temp_crc;
    V1D coef_m(coef.size()+m.size());
    V1D coef_m_crc(coef.size()+m.size()+crc_size);
    V1D m_crc8(m.size()+crc_size);

    // std::cout << "num_bit_error: " << n << std::endl;
    // std::cout << "m_1 at correction: ";
    // for(auto s : m)
    //     std::cout << +s;
    // std::cout << std::endl;

    for(int i=0;i<(1<<n);i++){
        temp_crc.clear();
        for(int j=0;j<n;j++){
            // cout << ((i & (1<<j))?"1":"0");
            if(i&(1<<j))
                temp[j] = 1;
            else
                temp[j] = 0;
        }

        // for(auto s : temp)
        //     std::cout << +s;
        // std::cout<<std::endl;

        int k=0;
        for(int i=0;i<error_location.size();++i){
            if(error_location[i] == 1){
                m[i] = temp[k];
                ++k;
            }
        }

        for(auto i=0; i < coef.size(); ++i){
            coef_m[i]= coef[i];
        }

        for(auto i=0; i < m.size(); ++i){
            coef_m[i+coef.size()] = m[i];
        }

        // std::cout << "coef_m: ";
        // for(auto s : coef_m)
        //     std::cout << +s;
        // std::cout << std::endl;

        for(auto i=0;i < m.size();++i){
            if(m[i] == 0)
                m[i] = 48;
            else if(m[i] == 1)
                m[i] = 49;
        }

        uint32_t int_crc = crc.crc8(m.data(), m.size());
        // std::cout << "int_crc: "<<std::hex << int_crc << std::endl;
        std::string str_crc = utils.int_to_binary8(int_crc);


        for(auto s : str_crc){
            if(s==48)
                s = 0;
            else if(s==49)
                s = 1;
            else
                std::cout << "Error in converting to binary at add_crc" << std::endl;
            temp_crc.push_back(s);
        }

        // for(auto s : temp_crc)
        //     std::cout << +s;
        // std::cout << std::endl;

        // for(auto s : crc_m)
        //     std::cout << +s;
        // std::cout << std::endl;

        if(crc_m == temp_crc){

            for(auto i=0;i<m.size();++i){
                m_crc8[i] = m[i];
                if(m[i] == 48)
                    m_crc8[i] = 0;
                if(m[i] == 49)
                    m_crc8[i] = 1;
            }

            for(auto i=0;i<temp_crc.size();++i)
                m_crc8[i+m.size()] = temp_crc[i];

            break;
        }
    }
    // std::cout << std::endl;

    // cout << "m_crc8: " << endl;
    // for(auto s : m_crc8)
    //     cout << +s;
    // cout << endl;
    return m_crc8;
}

V2D prac::guess_uncoded(V2D &m, uint32_t &seg_size)
{
    auto field = fifi::api::create_default_field(fifi::api::field::binary);

    utils utils;
    utils_prac utils_prac;

    uint32_t row = m.size();
    uint32_t col = m[0].size();

    uint32_t crc_size = 32;
    uint32_t crc8_size = 8;
    uint32_t num_seg = (col-row-crc_size) / (seg_size+crc8_size);

    V2D encoded(row,V1D(col-row-crc_size));
    V2D uncoded_crc8(row,V1D(col-row-crc_size));
    V2D uncoded(row,V1D(num_seg*seg_size));
    V2D coef_encoded(row,V1D(col-crc_size));
    V2D coef(row,V1D(row));
    V2D crc_m(row,V1D(crc_size));
    V1D buffer;
    V2D inv_coef(row,V1D(row));

    auto corr_crc = utils_prac.check_crc_prac(m);
    auto num_corr_crc = std::accumulate(corr_crc.begin(), corr_crc.end(), 0);

    // fill coef matrix with coeficient vector
    for(size_t i=0; i < row; ++i){
        for(size_t j=0; j < row; ++j){
            coef[i][j] = m[i][j];
        }
    }

    // fill coef_encoded matrix with coeficient vector + encoded packets with crc8
    for(size_t i=0; i < row; ++i){
        for(size_t j=0; j < col-crc_size; ++j){
            coef_encoded[i][j] = m[i][j];
        }
    }

    // std::cout << "coding vector: " << std::endl;
    // for(size_t k=0; k < row; ++k){
    //     for(size_t l=0; l < row ; ++l){
    //         std::cout << +coef[k][l];
    //     }
    //     std::cout << std::endl;
    // }

    // fill encoded matrix with encoded packet
    for(size_t i=0; i < row; ++i){
        for(size_t j=row; j < col-crc_size; ++j){
            encoded[i][j-row] = m[i][j];
        }
    }

    // std::cout << "encoded vector: " << std::endl;
    // for(size_t k=0; k < encoded.size(); ++k){
    //     for(size_t l=0; l < encoded[k].size(); ++l){
    //         std::cout << +encoded[k][l];
    //     }
    //     std::cout << std::endl;
    // }

    inv_coef = utils.calculate_inverse(coef);

    // std::cout << "inverse vector: " << std::endl;
    // for(size_t k=0; k < row; ++k){
    //     for(size_t l=0; l < row; ++l){
    //         std::cout << +inv_coef[k][l];
    //     }
    //     std::cout << std::endl;
    // }
    // std::cout << std::endl;

    uint32_t rowc = coef.size();
    uint32_t colc = coef[0].size();
    uint32_t rowp = uncoded_crc8.size();
    uint32_t colp = uncoded_crc8[0].size();

    // Matrix multiplication over GF(2) to get uncoded packet
    uint32_t coe,cod, ori;
    for(size_t i = 0; i < rowc; ++i)
    {
        for(size_t j = 0; j < colp; ++j)
        {
            for(size_t k = 0; k < colc; ++k)
            {
                uncoded_crc8[i][j] = field->add(uncoded_crc8[i][j], field-> multiply(inv_coef[i][k], encoded[k][j]));
            }
        }
    }

    // std::cout << std::endl;

    for(auto s=0;s<num_seg;++s){
        V2D m_seg(rowp,V1D(seg_size+crc8_size));

        for(auto j=0; j<uncoded_crc8.size(); ++j){
            for(auto k=(s*(seg_size+crc8_size)); k<((s+1)*(seg_size+crc8_size)); ++k){
                buffer.push_back(uncoded_crc8[j][k]);
            }
        }

        int it=0;
        for(auto i=0;i<m_seg.size();++i){
            for(auto j=0;j<m_seg[i].size();++j){
                m_seg[i][j] = buffer[it];
                ++it;
            }
        }

        for(auto i=0;i<m_seg.size();++i){
            m_seg[i].resize(seg_size);
        }

        for(auto i=0;i<uncoded.size();++i){
            for(auto j=(s*(seg_size));j<((s+1)*(seg_size));j++){
                uncoded[i][j] = m_seg[i][j-(s*(seg_size))];
            }
        }

        buffer.clear();
    }

    return uncoded;
}

V1D prac::compare_pkt(V1D &m1, V1D &m2)
{
    V1D error_location(m1.size());

    // std::bit_xor defined in <functional>
    std::transform(m1.begin(), m1.end(), m2.begin(), error_location.begin(), std::bit_xor<uint8_t>());

    return error_location;
}

// V1D prac::guess_encode(V1D &c, V2D &p)
// {
//     auto field = fifi::api::create_default_field(fifi::api::field::binary);

//     uint32_t rowc = 1;
//     uint32_t colc = c.size();

//     uint32_t rowp = p.size();
//     uint32_t colp = p[0].size();

//     V1D m(colp);

//     // Matrix multiplication over GF(2) between coding vector and
//     // uncoded packets to form encoded packets
//     uint32_t coe,cod, ori;
//     for(size_t i = 0; i < rowc; ++i)
//         for(size_t j = 0; j < colp; ++j)
//             for(size_t k = 0; k < rowp; ++k)
//             {
//                 // if(p[k][j] == 48)
//                 //     ori = 0;
//                 // else
//                 //     ori = 1;
//                 ori = p[k][j];

//                 coe = c[k];
//                 cod = m[j];

//                 m[j] = field->add(cod, field-> multiply(coe, ori));
//             }

//     std::cout << "m packet: " << std::endl;
//     for(size_t k=0; k < m.size(); ++k)
//     {
//         std::cout << +m[k];
//     }
//     std::cout <<"\n"<< std::endl;

//     V1D coef_m(c.size()+colp,0);

//     for(size_t i=0; i < c.size(); ++i)
//         coef_m[i] = c[i];

//     for(size_t i=c.size(); i < coef_m.size(); ++i)
//         coef_m[i] = m[i-c.size()];

//     std::cout << "coef_m packet: " << std::endl;
//     for(size_t k=0; k < coef_m.size(); ++k)
//     {
//         std::cout << +coef_m[k];
//     }
//     std::cout <<"\n"<< std::endl;

//     return m;
// }



// V1D prac::correction(V1D &coef, V1D &m, V1D &crc_m, V1D &error_location)
// {
//     crc crc;
//     utils utils;
//     uint32_t crc_size = 32;

//     int n = std::accumulate(error_location.begin(), error_location.end(),0);
//     V1D temp(n);
//     V1D temp_crc;
//     V1D coef_m(coef.size()+m.size());
//     V1D coef_m_crc(coef.size()+m.size()+crc_size);

//     std::cout << "num_bit_error: " << n << std::endl;
//     std::cout << "m_1 at correction: ";
//     for(auto s : m)
//         std::cout << +s;
//     std::cout << std::endl;

//     for(int i=0;i<(1<<n);i++){
//         temp_crc.clear();
//         for(int j=0;j<n;j++){
//             // cout << ((i & (1<<j))?"1":"0");
//             if(i&(1<<j))
//                 temp[j] = 1;
//             else
//                 temp[j] = 0;
//         }

//         int k=0;
//         for(int i=0;i<error_location.size();++i){
//             if(error_location[i] == 1){
//                 m[i] = temp[k];
//                 ++k;
//             }
//         }

//         for(auto i=0; i < coef.size(); ++i){
//             coef_m[i]= coef[i];
//         }

//         for(auto i=0; i < m.size(); ++i){
//             coef_m[i+coef.size()] = m[i];
//         }

//         // std::cout << "coef_m: ";
//         // for(auto s : coef_m)
//         //     std::cout << +s;
//         // std::cout << std::endl;

//         for(auto i=0;i < coef_m.size();++i){
//             if(coef_m[i] == 0)
//                 coef_m[i] = 48;
//             else if(coef_m[i] == 1)
//                 coef_m[i] = 49;
//         }

//         uint32_t int_crc = crc.crc32(coef_m.begin(), coef_m.end());
//         // std::cout << "int_crc: "<<std::hex << int_crc << std::endl;
//         std::string str_crc = utils.int_to_binary(int_crc);

//         for(auto s : str_crc){
//             if(s==48)
//                 s = 0;
//             else if(s==49)
//                 s = 1;
//             else
//                 std::cout << "Error in converting to binary at add_crc" << std::endl;
//             temp_crc.push_back(s);
//         }

//         // for(auto s : temp_crc)
//         //     std::cout << +s;
//         // std::cout << std::endl;

//         // for(auto s : crc_m)
//         //     std::cout << +s;
//         // std::cout << std::endl;

//         if(crc_m == temp_crc){

//             for(auto i=0;i<coef_m.size();++i){
//                 coef_m_crc[i] = coef_m[i];
//                 if(coef_m[i] == 48)
//                     coef_m_crc[i] = 0;
//                 if(coef_m[i] == 49)
//                     coef_m_crc[i] = 1;
//             }

//             for(auto i=0;i<temp_crc.size();++i)
//                 coef_m_crc[i+coef.size()+coef_m.size()] = temp_crc[i];

//             break;
//         }
//     }
//     std::cout << std::endl;
//     return coef_m_crc;
// }
