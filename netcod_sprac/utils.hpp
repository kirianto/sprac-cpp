#pragma once

#include <cmath>
#include <cstdint>
#include <vector>
#include <bitset>
#include <string>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <iterator>

#include "crc.hpp"

typedef std::vector<uint8_t> V1D;
typedef std::vector<V1D> V2D;

using namespace std;

class utils{
public:
    utils();
    ~utils();
    V2D calculate_inverse(V2D &);
    V2D vec_2dvector(V1D &, uint32_t &, uint32_t &);
    V2D add_crc(V2D &);
    V2D add_crc8(V2D &, uint32_t &);
    V1D check_crc(V2D &, V2D &);
    std::string int_to_binary(uint32_t &);
    std::string int_to_binary8(uint32_t &);
    std::string binary_to_hex(std::string &);
    std::string vector_to_str(V1D &);
    std::string bin_vec_to_char( V2D &);
    // V2D str_2dvector(std::string &, uint32_t &, uint32_t &);
    // V1D strToBinVec(std::string &);
    // bool correct_crc(V2D &, V2D &);

};

utils::utils(){ }

utils::~utils(){ }

// convert integer to binary string
// &n: integer number
// output: binary string 32 bits
std::string utils::int_to_binary(uint32_t &n)
{
    std::string str;
    uint32_t mask = 1 << (sizeof(int) * 8 - 1);

    for(int i = 0; i < sizeof(int) * 8; i++)
    {
        if( (n & mask) == 0 )
            str += '0';
        else
            str += '1';

        mask  >>= 1;
    }
    // return binary string
    return str;
}

// convert integer to binary string
// &n: integer number
// output: binary string 8 bits
std::string utils::int_to_binary8(uint32_t &n)
{
    std::string str;
    str = std::bitset<8>(n).to_string();

    // return binary string 8 bits
    return str;
}

// calculate inverse matrix
// &A: 2D coefficient vectors
// output: 2D inverse coefficient vectors
V2D utils::calculate_inverse(V2D &A)
{
    int n = A.size();

    V2D B(n, V1D(n,0));
    std::vector<std::vector<double>> temp(n, std::vector<double>(2*n,0));

    for(size_t i=0; i < n; ++i)
        for(size_t j=0; j < n; ++j)
            temp[i][j] = A[i][j];

    for (int i=0; i<n; i++)
        temp[i][n+i] = 1;

    for (int i=0; i<n; i++) {
        // Search for maximum in this column
        double maxEl = abs(temp[i][i]);
        int maxRow = i;
        for (int k=i+1; k<n; k++) {
            if (abs(temp[k][i]) > maxEl) {
                maxEl = temp[k][i];
                maxRow = k;
            }
        }

        // Swap maximum row with current row (column by column)
        for (int k=i; k<2*n;k++) {
            double tmp = temp[maxRow][k];
            temp[maxRow][k] = temp[i][k];
            temp[i][k] = tmp;
        }

        // Make all rows below this one 0 in current column
        for (int k=i+1; k<n; k++) {
            double c = -temp[k][i]/temp[i][i];
            for (int j=i; j<2*n; j++) {
                if (i==j) {
                    temp[k][j] = 0;
                } else {
                    temp[k][j] += c * temp[i][j];
                }
            }
        }
    }

    // Solve equation Ax=b for an upper triangular matrix A
    for (int i=n-1; i>=0; i--) {
        for (int k=n; k<2*n;k++) {
            temp[i][k] /= temp[i][i];
        }
        // this is not necessary, but the output looks nicer:
        temp[i][i] = 1;

        for (int rowModify=i-1;rowModify>=0; rowModify--) {
            for (int columModify=n;columModify<2*n;columModify++) {
                temp[rowModify][columModify] -= temp[i][columModify]
                                             * temp[rowModify][i];
            }
            // this is not necessary, but the output looks nicer:
            temp[rowModify][i] = 0;
        }
    }

    for(int i=0; i<n ; ++i){
        for(int j=n; j<n*2; ++j){
            if(temp[i][j] == 255)
                B[i][j-n] = 1;
            else
                B[i][j-n] = abs(temp[i][j]);
        }
    }

    // return inverse matrix
    return B;
}

// convert 1D vector to 2D vector
// input: payload, num of symbols, symbol size
// output : 2D vector of payload
V2D utils::vec_2dvector(V1D &payld, uint32_t &sym, uint32_t &sym_size)
{
    std::bitset<8> buffer;
    V2D vec2d(sym, V1D(sym_size*8));
    V1D vec;
    std::string str(payld.begin(), payld.end());

    for (std::size_t i = 0; i < str.size(); ++i)
    {
        buffer = (str.c_str()[i]);

        std::string mystring =
        buffer.to_string<char,std::string::traits_type,std::string::allocator_type>();

        for (size_t j=0; j < mystring.size(); ++j)
            vec.push_back(mystring.at(j));
    }

    int k = 0;
    for(size_t i=0; i < vec2d.size(); ++i)
    {
        for(size_t j=0; j < vec2d[0].size(); ++j)
        {
            vec2d[i][j] = vec[k];
            ++k;
        }
    }

    // return 2D vector of payload
    return vec2d;
}

// input &m: coef + encoded
// output: 2D vector of crc32 of each packets
V2D utils::add_crc(V2D &m)
{
    crc crc;

    uint32_t row = m.size();
    uint32_t col = m[0].size();
    uint32_t crc_size = 32; // in bits

    // create 2D buffer for crc of &m vectors
    V2D crc_m(row, V1D(crc_size));

    // temporary of new crc in 1D vectors
    V1D temp_crc;

    // buffer for temp of coef + encoded vectors
    V1D temp(col);

    uint32_t int_crc;
    std::string str_crc;

    for(size_t i=0; i < row; ++i){
        for(size_t j=0; j < col; ++j){
            // fill temp with coef + encoded vectors
            temp[j] = m[i][j];
        }

        for(auto i=0;i < temp.size();++i){
            if(temp[i] == 0)
                temp[i] = 48;
            else if(temp[i] == 1)
                temp[i] = 49;
        }

        // check crc and add it to int_crc
        int_crc = crc.crc32(temp.begin(), temp.end());

        // convert to binary
        str_crc = this->utils::int_to_binary(int_crc);

        for(auto s : str_crc){
            if(s==48)
                s = 0;
            else if(s==49)
                s = 1;
            else
                std::cout << "Error in converting to binary at add_crc" << std::endl;
            // add binary crc to temp_crc
            temp_crc.push_back(s);
        }
    }

    int k = 0;
    for(size_t i=0; i < crc_m.size(); ++i){
        for(size_t j=0; j < crc_m[0].size(); ++j){
            // add 2D crc_m with 1D temp_crc
            crc_m[i][j] = temp_crc[k];
            ++k;
        }
    }
    // return 2D vectors of CRC32 of each packets
    return crc_m;
}

// check crc 32
// &m: input of encoded packets
// &crc32: input of crc32
// ouput: 1D vector of correct crc32
V1D utils::check_crc(V2D &m, V2D &crc32)
{
    bool status;

    uint32_t row = m.size();
    uint32_t col = m[0].size();
    uint8_t crc_size = 32;

    V1D crc_correct(row);
    V2D crc_m(row, V1D(crc_size));

    // make crc32 of encoded packets
    crc_m = this->utils::add_crc(m);

    V1D temp_crc_m(crc_size);
    V1D temp_crc32(crc_size);

    std::cout << "encoded vector at check crc: " << std::endl;
    for(int i=0; i < row; ++i){
        for(int j=0; j < m[0].size(); ++j){
            std::cout << +m[i][j];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    std::cout << "Crc32 at check crc: " << std::endl;
    for(int i=0; i < row; ++i){
        for(int j=0; j < crc_size; ++j){
            std::cout << +crc32[i][j];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for(size_t i=0; i < row; ++i){
        for(size_t j=0; j < crc_size; ++j){
            temp_crc_m[j] = crc_m[i][j];
            temp_crc32[j] = crc32[i][j];
        }

        std::string str,hex,str32,hex32;

        str = this->utils::vector_to_str(temp_crc_m);
        hex = this->utils::binary_to_hex(str);
        str32 = this->utils::vector_to_str(temp_crc32);
        hex32 = this->utils::binary_to_hex(str32);

        if(hex == hex32)
            crc_correct[i] = 1;
        else
            crc_correct[i] = 0;
    }

    // return 1D vector of correct crc32
    return crc_correct;
}

// convert from binary string to hexadecimal
// &num: input of binary string
std::string utils::binary_to_hex(std::string &num)
{
    std::bitset<32> set(num);
    std::stringstream res;
    res << std::hex << std::uppercase << set.to_ulong();

    // return hexadecimal
    return res.str();
}

// convert from binary vector to binary string
// &vec: input of binary vectors
// output: binary string
std::string utils::vector_to_str(V1D &vec)
{
    std::ostringstream oss;

    if (!vec.empty())
    {
    // Convert all but the last element to avoid a trailing ","
        std::copy(vec.begin(), vec.end(),
            std::ostream_iterator<int>(oss, ""));
    }

    std::string s = oss.str();

    // return binary string
    return s;
}

// convert from binary vector to string
// &vec: input of 2D binary vector
std::string utils::bin_vec_to_char(V2D &vec)
{
    std::string char_out;
    for(int i=0; i < vec.size(); ++i){
        for(int j=0; j < vec[0].size(); ++j){
            if(vec[i][j] == 0)
                char_out += '0';
            else
                char_out += '1';
        }

    }
    // Allows the sequence of strings to be accessed directly as a string.
    std::istringstream in( char_out );
    std::string s;

    // Divides the bits into bytes.
    std::bitset<8> bs;

    // B-A Conversion
    // Loop extracts the bytes in the string and prints them as there ascii values.
    while ( in >> bs )
    {
        s += char( bs.to_ulong() );
    }

    // return binary string
    return s;
}

// make crc8 of encoded packets for a segment
// &m: input of encoded packets
// &seg_size: input of segment size
// ouput: 2D vector of crc8 for a segment
V2D utils::add_crc8(V2D &m, uint32_t &seg_size)
{
    crc crc;

    uint32_t row = m.size();
    uint32_t col = m[0].size();
    uint32_t crc_size = 8;
    uint32_t num_seg = col / seg_size;

    V1D temp;
    V2D m_crc8(row, V1D(num_seg*(seg_size+crc_size)));
    V2D temp_m(row*num_seg, V1D(seg_size));
    V2D temp_m_crc8(row*num_seg, V1D(seg_size+crc_size));
    V2D crc8(row*num_seg, V1D(crc_size));

    for(auto i=0; i<m.size(); ++i){
        for(auto j=0; j<m[i].size(); ++j){
            temp.push_back(m[i][j]);
        }
    }

    int k = 0;
    for(auto i=0; i<temp_m.size(); ++i){
        for(auto j=0; j<temp_m[i].size(); ++j){
            temp_m[i][j] = temp[k];
            k++;
        }
    }
    k=0;
    temp.clear();

    uint32_t int_crc;
    std::string str_crc;

    for(auto i=0; i<temp_m.size(); ++i){
        for(auto j=0; j < temp_m[i].size();++j){
            if(temp_m[i][j] == 0)
                temp_m[i][j] = 48;
            else if(temp_m[i][j] == 1)
                temp_m[i][j] = 49;
        }

        // check crc32 of coef + encoded vectors
        // store it in int_crc for converting to binary
        int_crc = crc.crc8(temp_m[i].data(), temp_m[i].size());

        str_crc = std::bitset<8>(int_crc).to_string();

        for(auto s: str_crc){
            if(s==48)
                s = 0;
            else
                s = 1;

            // store binary crc to crc_m vectors
            temp.push_back(s);
        }

        for(auto j=0; j < temp_m[i].size();++j){
            if(temp_m[i][j] == 48)
                temp_m[i][j] = 0;
            else if(temp_m[i][j] == 49)
                temp_m[i][j] = 1;
        }
    }

    for(auto i=0; i< crc8.size(); i++){
        for(auto j=0; j < crc8[i].size(); ++j){
            crc8[i][j] = temp[k];
            k++;
        }
    }
    k=0;
    temp.clear();

    for(auto i=0; i< temp_m_crc8.size(); i++){
        for(auto j=0; j < (temp_m_crc8[i].size()-crc_size); ++j){
            temp_m_crc8[i][j] = temp_m[i][j];
        }

        for(auto j=seg_size; j < temp_m_crc8[i].size(); ++j){
            temp_m_crc8[i][j] = crc8[i][j-seg_size];
        }
    }

    for(auto i=0; i<temp_m_crc8.size(); i++){
        for(auto j=0; j < temp_m_crc8[i].size(); ++j){
            temp.push_back(temp_m_crc8[i][j]);
        }
    }

    for(auto i=0; i < m_crc8.size(); ++i){
        for(auto j=0; j < m_crc8[i].size(); ++j){
            m_crc8[i][j] = temp[k];
            k++;
        }
    }
    k=0;
    temp.clear();

    // return 2D vector of crc8 for a segment
    return m_crc8;
}

// // check correct crc
// // &m: input of coef + encoded vectors
// // &crc32: input of default crc32
// bool utils::correct_crc(V2D &m, V2D &crc32)
// {

//     bool status;

//     uint32_t row = m.size();
//     uint32_t col = m[0].size();

//     uint8_t crc_size = 32;

//     V2D crc_m(row, V1D(crc_size));

//     crc_m = this->utils::add_crc(m);

//     status = std::equal(crc_m.begin(), crc_m.end(), crc32.begin());

//     // return status of correct crc
//     return status;
// }

// // convert from string to binary vectors
// // &str: input of string
// // output: 1D vector of binary
// V1D utils::strToBinVec(std::string &str)
// {
//     std::bitset<8> buffer;
//     V1D vec;
//     for (std::size_t i = 0; i < str.size(); ++i)
//     {
//         buffer = (str.c_str()[i]);

//         std::string mystring =
//         buffer.to_string<char,std::string::traits_type,std::string::allocator_type>();

//         for (size_t j=0; j < mystring.size(); ++j)
//             vec.push_back(mystring.at(j));
//     }
//     // return binary 1D vector from string
//     return vec;
// }

// // convert from string to binary and form it to 2D vector
// // &in: input string
// // &symbols: number of symbols
// // &symbol_size: number of symbol size
// // output: 2D vectors of binary
// V2D utils::str_2dvector(std::string &in, uint32_t &symbols, uint32_t &symbol_size)
// {
//     V1D vec;
//     V2D vec2d(symbols, V1D(symbol_size*8));

//     vec = strToBinVec(in);

//     int k = 0;
//     for(size_t i=0; i < vec2d.size(); ++i)
//     {
//         for(size_t j=0; j < vec2d[0].size(); ++j)
//         {
//             vec2d[i][j] = vec[k];
//             ++k;
//         }
//     }

//     // return binary 2D vector in form of packets
//     return vec2d;
// }