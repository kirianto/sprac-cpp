#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

#include "crc.hpp"
#include "utils.hpp"

typedef std::vector<uint8_t> V1D;
typedef std::vector<V1D> V2D;

class utils_prac{
public:
    utils_prac();
    ~utils_prac();
    V1D check_crc_prac(V2D &);
    V1D check_crc8_prac(V2D &);
    V2D make_crc8(V2D &);
    bool status_crc_prac(V1D &);
};

utils_prac::utils_prac(){ }

utils_prac::~utils_prac(){ }

// check crc
// m_crc: 2D vectors of coef + encoded + crc32
// output:  1D vectors of crc of each packets
V1D utils_prac::check_crc_prac(V2D &m_crc)
{
    bool status;
    utils utils;

    uint32_t row = m_crc.size(); // rows of m_crc vectors
    uint32_t col = m_crc[0].size(); // column of m_crc vectors

    uint8_t crc_size = 32; // in bits

    V1D crc_correct(row);

    // create buffer for coef + encoded vectors
    V2D coef_m(row, V1D(col-crc_size));

    // create buffer for crc32 vectors
    V2D crc_coef_m(row, V1D(crc_size));

    // create buffer for crc32 vectors
    V2D crc_m(row, V1D(crc_size));

    // fill coef_m with coef + encoded vectors
    for(int i=0; i < row; ++i){
        for(int j=0; j < col-crc_size; ++j){
            coef_m[i][j] = m_crc[i][j];
        }
    }

    // fill crc_coef_m with crc32
    for(int i=0; i < row; ++i){
        for(int j=col-crc_size; j < col; ++j){
            crc_coef_m[i][j-(col-crc_size)] = m_crc[i][j];
        }
    }

    // check crc of coef_m and add it to crc_m
    crc_m = utils.add_crc(coef_m);

    // create buffer for new crc of encoded packets
    V1D temp_crc_m(crc_size);

    // create buffer for default crc
    V1D temp_crc32(crc_size);

    for(size_t i=0; i < row; ++i){
        for(size_t j=0; j < crc_size; ++j){
            temp_crc_m[j] = crc_m[i][j];
            temp_crc32[j] = crc_coef_m[i][j];
        }

        std::string str,hex,str32,hex32;

        str = utils.vector_to_str(temp_crc_m);
        hex = utils.binary_to_hex(str);
        str32 = utils.vector_to_str(temp_crc32);
        hex32 = utils.binary_to_hex(str32);

        // compare new checked crc and default crc
        if(hex == hex32)
            crc_correct[i] = 1;
        else
            crc_correct[i] = 0;
    }

    // return correct crc
    return crc_correct;
}

// return status of packet crc
// &crc: 1D vector of correct crc of each packets
bool utils_prac::status_crc_prac(V1D &crc)
{
    bool status;

    uint32_t sum;

    sum = std::accumulate(crc.begin(), crc.end(), 0);

    if(sum == crc.size())
        status = 1;
    else
        status = 0;

    return status;
}

// return 2D vector of crc8 of encoded packets for segments
// &m: 2D vector of encoded packets
V2D utils_prac::make_crc8(V2D &m)
{
    crc crc;

    uint32_t row = m.size();
    uint32_t col = m[0].size();
    uint32_t crc_size = 8;
    uint32_t int_crc;
    int k=0;

    std::string str_crc;

    V2D crc8(row, V1D(crc_size));
    V1D temp_m;
    V1D temp;
    V2D temp_crc;

    for(auto i=0; i<m.size();++i){
        for(auto j=0; j<m[i].size(); ++j){
            temp_m.push_back(m[i][j]);
        }
        for(auto i=0; i<temp_m.size();++i){
            if(temp_m[i] == 0)
                temp_m[i] = 48;
            else if(temp_m[i] == 1)
                temp_m[i] = 49;
        }

        // make crc8
        int_crc = crc.crc8(temp_m.data(), temp_m.size());

        // convert int of crc8 to binary string
        str_crc = std::bitset<8>(int_crc).to_string();

        for(auto s: str_crc){
            if(s==48)
                s = 0;
            else
                s = 1;

            // store binary crc to crc_m vectors
            temp.push_back(s);
        }

        temp_m.clear();
    }

    for(auto i=0; i<crc8.size();++i){
        for(auto j=0; j<crc8[i].size();++j){
            crc8[i][j] = temp[k];
            ++k;
        }
    }
    k=0;

    // return crc8 of encoded packets for segments
    return crc8;
}

// return 1D vector of correct crc8 of a segment
// &m_crc8: encoded packets with crc8
V1D utils_prac::check_crc8_prac(V2D &m_crc8)
{
    bool status;
    utils utils;

    uint32_t row = m_crc8.size(); // rows of m_crc vectors
    uint32_t col = m_crc8[0].size(); // column of m_crc vectors

    uint8_t crc_size = 8; // in bits of crc8

    V1D crc_correct(row);

    // create buffer for encoded vectors
    V2D m(row, V1D(col-crc_size));

    // create buffer for default crc8 vectors
    V2D crc8(row, V1D(crc_size));

    // create buffer for new crc8 vectors
    V2D crc8_m(row, V1D(crc_size));

    // fill m with encoded vectors
    for(int i=0; i < row; ++i){
        for(int j=0; j < col-crc_size; ++j){
            m[i][j] = m_crc8[i][j];
        }
    }

    // fill crc_coef_m with crc8
    for(int i=0; i < row; ++i){
        for(int j=col-crc_size; j < col; ++j){
            crc8[i][j-(col-crc_size)] = m_crc8[i][j];
        }
    }

    // // check crc of coef_m and add it to crc_m
    crc8_m = this->make_crc8(m);

    // create buffer for new crc of encoded packets
    V1D temp_crc_m(crc_size);

    // create buffer for default crc8
    V1D temp_crc8(crc_size);

    for(size_t i=0; i < row; ++i){
        for(size_t j=0; j < crc_size; ++j){
            temp_crc_m[j] = crc8_m[i][j];
            temp_crc8[j] = crc8[i][j];
        }

        std::string str,hex,str32,hex32;

        str = utils.vector_to_str(temp_crc_m);
        hex = utils.binary_to_hex(str);
        str32 = utils.vector_to_str(temp_crc8);
        hex32 = utils.binary_to_hex(str32);

        // compare new checked crc and default crc
        if(hex == hex32)
            crc_correct[i] = 1;
        else
            crc_correct[i] = 0;
    }
    // return correct crc8
    return crc_correct;
}