#include <cstdint>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <chrono>
#include <istream>
#include <fstream>
#include <cmath>

#include "netcod_sprac/utils.hpp"
#include "netcod_sprac/utils_prac.hpp"
#include "netcod_sprac/coder.hpp"
#include "netcod_sprac/prac.hpp"

typedef std::vector<uint8_t> V1D;
typedef std::vector<uint32_t> V1D32;
typedef std::vector<V1D> V2D;
typedef std::vector<V1D32> V2D32;
typedef std::chrono::duration<double, std::milli> T_milli;
typedef std::vector<std::chrono::duration<double, std::milli>> V1DT;

V2D flipped_bits(V1D32 input_a, V1D32 input_b, V2D &coded, uint32_t &symbols, uint32_t &crc_size);

int main()
{
    encoder encoder;
    decoder decoder;
    utils utils;
    utils_prac utils_prac;
    prac prac;

    V2D coef{   {1,0,0},
                {0,1,0},
                {0,0,1},
                {1,1,1}
            };

    // V2D coef{   {1,0,0,0,0},{0,1,0,0,0},{0,0,1,0,0},
    //             {0,0,0,1,0},{0,0,0,0,1},{1,1,1,1,1}
    //         };

    // V2D coef{   {1,0,0,0,0,0,0},{0,1,0,0,0,0,0},
    //             {0,0,1,0,0,0,0},{0,0,0,1,0,0,0},
    //             {0,0,0,0,1,0,0},{0,0,0,0,0,1,0},
    //             {0,0,0,0,0,0,1},{1,1,1,1,1,1,1}
    //         };

    uint32_t symbols = coef[0].size();
    uint32_t symbol_size = 20;
    uint32_t runs = 10000;

    uint32_t cod_vec = symbols + 1;
    uint32_t crc_size = 32;
    uint32_t crc8_size = 8;
    uint32_t seg_size = 8; // 32 in bits --> equal to 4 bytes
    uint32_t num_seg = (symbol_size*8)/seg_size;

    std::vector<uint8_t> data_in(symbols*symbol_size);
    std::generate(data_in.begin(), data_in.end(),rand);

    V2D32 flipped_a{
        {(symbols+17)},
        {(symbols+17),(symbols+35)},
        {(symbols+17),(symbols+35),(symbols+53)},
        {(symbols+17),(symbols+35),(symbols+53),(symbols+80)},
        {(symbols+17),(symbols+35),(symbols+53),(symbols+80),(symbols+98)},
        // {(symbols+17),(symbols+35),(symbols+53),(symbols+80),(symbols+98),(symbols+116)},
        // {(symbols+17),(symbols+35),(symbols+53),(symbols+80),(symbols+98),(symbols+116),(symbols+134)},
    };

    // // seg_size: 32
    // V2D32 flipped_a{
    //     {(symbols+10)},
    //     {(symbols+10),(symbols+20)},
    //     {(symbols+10),(symbols+20),(symbols+30)},
    //     {(symbols+10),(symbols+20),(symbols+30),(symbols+48)},
    //     {(symbols+10),(symbols+20),(symbols+30),(symbols+48),(symbols+58)},
    //     {(symbols+10),(symbols+20),(symbols+30),(symbols+48),(symbols+58),(symbols+68)},
    //     {(symbols+10),(symbols+20),(symbols+30),(symbols+48),(symbols+58),(symbols+68),(symbols+98)},
    // };

    V2D32 flipped_b{
        {(symbols+18)},
        {(symbols+18),(symbols+36)},
        {(symbols+18),(symbols+36),(symbols+55)},
        {(symbols+18),(symbols+36),(symbols+55),(symbols+81)},
        {(symbols+18),(symbols+36),(symbols+55),(symbols+81),(symbols+99)},
        // {(symbols+18),(symbols+36),(symbols+55),(symbols+81),(symbols+99),(symbols+117)},
        // {(symbols+18),(symbols+36),(symbols+55),(symbols+81),(symbols+99),(symbols+117),(symbols+135)},
    };

    // // seg_size: 32
    // V2D32 flipped_b{
    //     {(symbols+5)},
    //     {(symbols+5),(symbols+15)},
    //     {(symbols+5),(symbols+15),(symbols+25)},
    //     {(symbols+5),(symbols+15),(symbols+25),(symbols+43)},
    //     {(symbols+5),(symbols+15),(symbols+25),(symbols+43),(symbols+53)},
    //     {(symbols+5),(symbols+15),(symbols+25),(symbols+43),(symbols+53),(symbols+63)},
    //     {(symbols+5),(symbols+15),(symbols+25),(symbols+43),(symbols+53),(symbols+63),(symbols+81)},
    // };

    V2D coded(cod_vec, V1D((symbols)+(symbol_size*8)+(num_seg*crc8_size)+crc_size,0));
    V2D decoded(symbols, V1D((symbols)+(symbol_size*8)+(num_seg*crc8_size)+crc_size,0));
    V2D uncoded(symbols, V1D(symbol_size*8));
    V2D data_out(symbols, V1D(symbol_size*8));
    V1D crc_correct(cod_vec);
    V2D m_out_prac;
    V1D temp;
    V1D vec_out;

    uncoded = utils.vec_2dvector(data_in, symbols, symbol_size);

    std::cout << "original packets: " << std::endl;
    for(size_t k=0; k < uncoded.size(); ++k){
        for(size_t l=0; l < uncoded[k].size() ; ++l){
            std::cout << uncoded[k][l];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    ofstream myfile;
    myfile.open("sprac_scheme_1_3.txt");
    // myfile <<   "Num_Err" << "\t" <<
    //             "Error_Loc_Time" << "\t" <<
    //             "Corr_Time" << "\t" <<
    //             "SPRAC_Time" << "\t" <<
    //             "Encod_Time" << "\t" <<
    //             "Decod_Time" << endl;

    for(auto s=0;s<flipped_a.size();s++){
        cout << "flipped: " << (flipped_a[s].size())*2 << endl;

        // T_milli err_loc_time, corr_time;
        V1DT err_loc_time(runs), corr_time(runs), diff_sprac(runs);
        T_milli tot_err_loc_time(0), tot_corr_time(0);
        T_milli avg_err_loc_time, avg_corr_time;
        T_milli tot_diff_encoding(0), tot_diff_decoding(0), tot_diff_sprac(0);
        T_milli avg_diff_encoding, avg_diff_decoding, avg_diff_sprac;
        double var_err_loc_time=0, var_corr_time=0, var_diff_sprac=0;
        double std_corr_time, std_err_loc_time, std_diff_sprac;

        for(auto i=0;i<runs;++i){

            vec_out.clear();
            temp.clear();

            auto start_encoding = std::chrono::high_resolution_clock::now();

            coded = encoder.encode(coef, uncoded, seg_size);

            auto end_encoding = std::chrono::high_resolution_clock::now();

            // Store the time difference between start and end
            auto diff_encoding = end_encoding - start_encoding;
            tot_diff_encoding += diff_encoding;

            coded = flipped_bits(flipped_a[s], flipped_b[s], coded,symbols,crc_size);

            crc_correct = utils_prac.check_crc_prac(coded);

            auto start_sprac = std::chrono::high_resolution_clock::now();

            if(!utils_prac.status_crc_prac(crc_correct)){
                std::tie(m_out_prac, err_loc_time[i], corr_time[i]) = prac.decode(coded, crc_correct, symbols, symbol_size, seg_size);
            } else{
                // decoder.decode(coded);
            }

            auto end_sprac = std::chrono::high_resolution_clock::now();

            // Store the time difference between start and end
            diff_sprac[i] = end_sprac - start_sprac;
            tot_diff_sprac += diff_sprac[i];

            tot_err_loc_time += err_loc_time[i];
            tot_corr_time += corr_time[i];

            // take packet 1,3,4 to be decoded and create temp vec to store
            // the encoded packets in 1d vec


            for(size_t i=0; i < cod_vec; ++i)
            {
                for(size_t j=0; j < m_out_prac[0].size(); ++j)
                {
                    temp.push_back(m_out_prac[i][j]);
                }
            }

            // converting 1d temp vec to 2d decoded vec
            int k = 0;
            for(size_t i=0; i < symbols; ++i)
            {
                for(size_t j=0; j < m_out_prac[0].size(); ++j)
                {
                    decoded[i][j] = temp[k];
                    ++k;
                }
            }

            auto start_decoding = std::chrono::high_resolution_clock::now();

            data_out = prac.guess_uncoded(decoded, seg_size);

            auto end_decoding = std::chrono::high_resolution_clock::now();

            // Store the time difference between start and end
            auto diff_decoding = end_decoding - start_decoding;
            tot_diff_decoding += diff_decoding;

            std::string str_out = utils.bin_vec_to_char(data_out);

            std::copy(str_out.begin(), str_out.end(), std::back_inserter(vec_out));

        }

        if(vec_out == data_in)
            std::cout << "Data is decoded correctly. Congratulations!" << std::endl;
        else
            std::cout << "Something is error. Cannot be decoded correctly." << std::endl;

        avg_corr_time = tot_corr_time / runs;
        avg_err_loc_time = tot_err_loc_time /runs;
        avg_diff_encoding = tot_diff_encoding / runs;
        avg_diff_decoding = tot_diff_decoding / runs;
        avg_diff_sprac = tot_diff_sprac / runs;

        for(auto i=0;i<runs;++i){
            var_corr_time += pow(corr_time[i].count()-avg_corr_time.count(),2);
            var_err_loc_time += pow(err_loc_time[i].count()-avg_err_loc_time.count(), 2);
            var_diff_sprac += pow(diff_sprac[i].count()-avg_diff_sprac.count(), 2);
        }

        var_corr_time = var_corr_time/runs;
        var_err_loc_time = var_err_loc_time/runs;
        var_diff_sprac = var_diff_sprac/runs;

        std_corr_time = sqrt(var_corr_time);
        std_err_loc_time = sqrt(var_err_loc_time);
        std_diff_sprac = sqrt(var_diff_sprac);

        myfile <<   flipped_a[s].size()*2 <<
                    "," << T_milli (avg_err_loc_time).count() <<
                    "," << T_milli (avg_corr_time).count() <<
                    "," << T_milli (avg_diff_sprac).count() <<
                    "," << std_err_loc_time <<
                    "," << std_corr_time <<
                    "," << std_diff_sprac <<
                    endl;

        std::cout << "err_loc time: ";
        std::cout << T_milli (avg_err_loc_time).count() << " ms" << std::endl;

        std::cout << "correction time: ";
        std::cout << T_milli (avg_corr_time).count() << " ms" << std::endl;

        std::cout << "sprac time: ";
        std::cout << T_milli (avg_diff_sprac).count() << " ms" << std::endl;

        cout << "std_err_loc_time: " << std_err_loc_time << endl;
        cout << "std_corr_time: " << std_corr_time << endl;
        cout << "std_diff_sprac: " << std_diff_sprac << endl;

        // std::cout << "encoding time: ";
        // std::cout << T_milli (avg_diff_encoding).count() << " ms" << std::endl;

        // std::cout << "decoding time: ";
        // std::cout << T_milli (avg_diff_decoding).count() << " ms" << std::endl;
        cout << endl;
    }
    myfile.close();
    return 0;
}

V2D flipped_bits(V1D32 input_a, V1D32 input_b, V2D &coded, uint32_t &symbols, uint32_t &crc_size){

    // bits flipped in coded packet of 1 and 2

    for(auto i=0;i<input_a.size();++i){
        for(auto k=0;k<coded.size();++k){
            if(k==0){
                for(auto j=symbols; j< (coded[k].size()-crc_size);++j){
                    if(j == input_a[i]){
                        if(coded[k][j] == 0)
                            coded[k][j] = 1;
                        else
                            coded[k][j] = 0;
                    }
                }
            }

            if(k==1){
                for(auto j=symbols; j< (coded[k].size()-crc_size);++j){
                    if(j == input_b[i]){
                        if(coded[k][j] == 0)
                            coded[k][j] = 1;
                        else
                            coded[k][j] = 0;
                    }
                }
            }
        }

    }

    return coded;
}